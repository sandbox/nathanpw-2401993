<?php
/**
 * @file
 * bean_share.features.defaultconfig.inc
 */

/**
 * Implements hook_defaultconfig_features().
 */
function bean_share_defaultconfig_features() {
  return array(
    'bean_share' => array(
      'user_default_permissions' => 'user_default_permissions',
    ),
  );
}

/**
 * Implements hook_defaultconfig_user_default_permissions().
 */
function bean_share_defaultconfig_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create any bean_share_type bean'.
  $permissions['create any bean_share_type bean'] = array(
    'name' => 'create any bean_share_type bean',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'bean',
  );

  // Exported permission: 'delete any bean_share_type bean'.
  $permissions['delete any bean_share_type bean'] = array(
    'name' => 'delete any bean_share_type bean',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'bean',
  );

  // Exported permission: 'edit any bean_share_type bean'.
  $permissions['edit any bean_share_type bean'] = array(
    'name' => 'edit any bean_share_type bean',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'bean',
  );

  // Exported permission: 'view any bean_share_type bean'.
  $permissions['view any bean_share_type bean'] = array(
    'name' => 'view any bean_share_type bean',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'bean',
  );

  return $permissions;
}
