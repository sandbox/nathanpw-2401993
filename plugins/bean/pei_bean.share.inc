<?php
/**
 * @file
 * Listing bean plugin.
 */

class BeanShare extends BeanPlugin {
  /**
   * Declares default block settings.
   */
  public function values() {
    $values = array(
      'share_settings' => array(
        'target' => '_self',
        'custom_css' => '',
      ),
    );

    return array_merge(parent::values(), $values);
  }

  /**
   * Builds extra settings for the block edit form.
   */
  public function form($bean, $form, &$form_state) {
    $form = array();

    $form['share_settings'] = array(
      '#type' => 'fieldset',
      '#title' => t('Configure Share Widget'),
      '#tree' => TRUE,
      '#weight' => -8,
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );

    $form['share_settings']['target'] = array(
      '#type' => 'select',
      '#title' => t('Select target'),
      '#options' => array(
        '_blank' => t('Open the link in a new window or tab'),
        '_self' => t('Open the link in the same frame as it was clicked (default)'),
        '_parent' => t('Open the link in the parent frame'),
        '_top' => t('Open the link in the full body of the window'),
      ),
      '#default_value' => isset($bean->share_settings['target']) ? $bean->share_settings['target'] : '_self',
    );

    $form['share_settings']['custom_css'] = array(
      '#type' => 'textfield',
      '#title' => t('Configure Shared Widget Custom CSS class'),
      '#default_value' => isset($bean->share_settings['custom_css']) ? $bean->share_settings['custom_css'] : '',
      '#size' => 60,
      '#maxlength' => 128,
      '#required' => FALSE,
    );

    return $form;
  }

  /**
   * Displays the bean.
   */
  public function view($bean, $content, $view_mode = 'default', $langcode = NULL) {

    $target = isset($bean->share_settings['target']) ? $bean->share_settings['target'] : '_self';
    $custom_css = isset($bean->share_settings['custom_css']) ? $bean->share_settings['custom_css'] : '';
    $datatext = drupal_get_title();
    $dataurl =  $GLOBALS['base_url'] . '/' .  request_path();
    // Reset content.
    $content = array();

    // Rendered markup.
    $markup = '<div class="clearfix">';
    $markup = '<div class="op-share ' . $custom_css . '">';
    $markup .= '
    <div class="like-block">
                <button class="like-button share s_twitter" title="' . t('Share this page on Twitter')  . '"></button>
                <div class="like-counter counter c_twitter"></div>
            </div>
            <div class="like-block">
                <button class="like-button share s_facebook" title="' . t('Share this page on Facebook')  . '"></button>
                <div class="like-counter counter c_facebook"></div>
            </div>

            <div class="like-block">
                <a href="mailto:?body=' . $dataurl . '&subject=' . $datatext . '" class="like-button share email" title="' . t('Email this page to a friend')  . '"></a>
            </div>

';


            
  
          
    $markup .= '</div>';
    $markup .= '</div>';

    $content['share_bean']['#markup'] = $markup;

    return $content;
  }
}
